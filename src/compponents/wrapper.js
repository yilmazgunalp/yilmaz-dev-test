const React = require('react');
const Button = require('./button');

function Wrapper() {
      
  return (
      <div>
        <div className='button-group'>
        <h2>default</h2>
        <Button label="Button"/><Button disabled={true} label="Button"/>
        </div>

        <div className='button-group'>
        <h2>primary</h2>
        <Button primary={true} label="Button"/><Button primary={true} disabled={true} label="Button"/>
        </div>

        <div className='button-group'>
        <h2>secondary</h2>
        <Button secondary={true} label="Button"/><Button secondary={true} disabled={true} label="Button"/>
        </div>

        <div className='button-group'>
        <h2>flat</h2>
        <Button flat={true} label="Button"/><Button flat={true} disabled={true} label="Button"/>
        </div>
      </div>
    ); 
}

module.exports = Wrapper;

