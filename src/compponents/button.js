import React, { Component } from 'react';
import './Button.css';


class Button extends Component {
	
 constructor(props) {
      super(props);
      this.state = {
			status: this.props.disabled?"disabled":""
        
      }
		this.disabled = this.props.disabled;
		let btn_cls = this.btn_cls = "default";
		if(this.props.primary){
			this.btn_cls = "primary";
		}else if(this.props.secondary){
			this.btn_cls = "secondary";
		}else if(this.props.flat){
			this.btn_cls = "flat";
		}else{
			this.btn_cls = "default";
		}
		console.log(this.btn_cls);
   }
   hover(){
	   console.log("disabled?"+this.disabled );
	   if(!this.disabled){
		this.setState({status:"hover"});
		console.log(this.state.status);
	   }
   }
   blur(){
	   if(!this.disabled){
		this.setState({status:""});
		console.log(this.state.status);
	   }
   }
 
  render() {
	  
    return (
		
		<div className={`button ${this.btn_cls} ${this.state.status}`} onMouseEnter={()=>this.hover()} onMouseOut={()=>this.blur()}>{this.props.label}</div>
		
		 
	);
	
  }
}

module.exports = Button;
